package pkg

import (
	"github.com/micro/go-micro/v2"
	"github.com/micro/go-micro/v2/registry"

)

//goland:noinspection ALL
func NewService(reg registry.Registry) proto.AuthenticationService {
	srv := micro.NewService(
		micro.Name(service.ServiceID),
		micro.Address(":8005"),
		micro.Registry(reg),
	)
	return proto.NewAuthenticationService(srv.Name(), srv.Client())
}
