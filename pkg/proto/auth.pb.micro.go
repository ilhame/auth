// Code generated by protoc-gen-micro. DO NOT EDIT.
// source: auth.proto

package proto

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

import (
	context "context"
	api "github.com/micro/go-micro/v2/api"
	client "github.com/micro/go-micro/v2/client"
	server "github.com/micro/go-micro/v2/server"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

// Reference imports to suppress errors if they are not otherwise used.
var _ api.Endpoint
var _ context.Context
var _ client.Option
var _ server.Option

// Api Endpoints for Authentication service

func NewAuthenticationEndpoints() []*api.Endpoint {
	return []*api.Endpoint{}
}

// Client API for Authentication service

type AuthenticationService interface {
	GetUser(ctx context.Context, in *Token, opts ...client.CallOption) (*User, error)
}

type authenticationService struct {
	c    client.Client
	name string
}

func NewAuthenticationService(name string, c client.Client) AuthenticationService {
	return &authenticationService{
		c:    c,
		name: name,
	}
}

func (c *authenticationService) GetUser(ctx context.Context, in *Token, opts ...client.CallOption) (*User, error) {
	req := c.c.NewRequest(c.name, "Authentication.GetUser", in)
	out := new(User)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// Server API for Authentication service

type AuthenticationHandler interface {
	GetUser(context.Context, *Token, *User) error
}

func RegisterAuthenticationHandler(s server.Server, hdlr AuthenticationHandler, opts ...server.HandlerOption) error {
	type authentication interface {
		GetUser(ctx context.Context, in *Token, out *User) error
	}
	type Authentication struct {
		authentication
	}
	h := &authenticationHandler{hdlr}
	return s.Handle(s.NewHandler(&Authentication{h}, opts...))
}

type authenticationHandler struct {
	AuthenticationHandler
}

func (h *authenticationHandler) GetUser(ctx context.Context, in *Token, out *User) error {
	return h.AuthenticationHandler.GetUser(ctx, in, out)
}
