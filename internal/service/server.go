package service

import (
	"context"
	"github.com/micro/go-micro/v2"
	"github.com/micro/go-micro/v2/logger"
	"github.com/micro/go-micro/v2/registry"
	"go.uber.org/fx"
)

const ServiceID = "auth"

func NewServer(lc fx.Lifecycle, reg registry.Registry, l logger.Logger) micro.Service {
	successChannel := make(chan int)
	errorChannel := make(chan error)

	srv := micro.NewService(
		micro.Name(ServiceID),
		micro.Address(":8005"),
		micro.Registry(reg),
		micro.AfterStart(func() error {
			successChannel <- 0
			return nil
		}),
	)
	srv.Init()

	lc.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			go func() {
				err := srv.Run()
				if err != nil {
					l.Log(logger.ErrorLevel, "Failed to start server")
					errorChannel <- err
				}
			}()
			select {
			case <-successChannel:
				return nil
			case err := <-errorChannel:
				return err
			case <-ctx.Done():
				return ctx.Err()
			}
		},
		OnStop: func(ctx context.Context) error {
			return srv.Server().Stop()
		},
	})
	return srv
}
