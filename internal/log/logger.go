package log

import (
	"github.com/micro/go-micro/v2/logger"
	zlogger "github.com/micro/go-plugins/logger/zap/v2"
)

// NewLogger initialize new logger and set it as a default logger for go-micro
func NewLogger() (logger.Logger, error) {
	l, err := zlogger.NewLogger()
	if err != nil {
		return nil, err
	}

	logger.DefaultLogger = l
	return l, nil
}
