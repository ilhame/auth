package authenticator

import (
	"context"
	"firebase.google.com/go/v4/auth"
)

type Repo interface {
	VerifyIDToken(ctx context.Context, idToken string) (*Token, error)
	GetUser(ctx context.Context, uid string) (*UserRecord, error)
}

func NewRepo(frb *auth.Client) Repo {
	return &Firebase{frb: frb}
}