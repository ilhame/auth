package config

import (
	"github.com/spf13/viper"
	"strings"
)

type Config struct {
	EtcdConfig     etcdConfig
	FirebaseConfig firebaseConfig
}

type etcdConfig struct {
	User     string
	Password string
	Address  string
}

type firebaseConfig struct {
	GoogleCredentialsPath string
}

func NewConfig() *Config {
	conf := viper.New()
	conf.AutomaticEnv()
	conf.SetEnvPrefix("auth")
	conf.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	return &Config{
		etcdConfig{
			User:     conf.GetString("etcd.user"),
			Password: conf.GetString("etcd.password"),
			Address:  conf.GetString("etcd.address"),
		},
		firebaseConfig{
			GoogleCredentialsPath: conf.GetString("firebase.sa"),
		},
	}
}